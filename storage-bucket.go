package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/iterator"
)

type StorageBucketNotFoundError struct {
	Path string
}

func (e *StorageBucketNotFoundError) Error() string {
	return fmt.Sprintf("object not found at path %v", e.Path)
}

type StorageBucket struct {
	client     *storage.Client
	bucketName string
	bucket     *storage.BucketHandle
	ctx        context.Context
}

func (s *StorageBucket) objectBucketPath(path string) string {
	logger.Printf("Building bucket path for /%s\n", path)

	return fmt.Sprintf("/gs/%s/%s", s.bucketName, path)
}

func (s *StorageBucket) objectBucketPathForPathMediaType(path string, mediaType MediaType) (string, error) {
	logger.Printf("Checking for %s at path %v", mediaType.String(), path)

	objectAttrs, _ := s.bucket.Object(path).Attrs(s.ctx)
	if objectAttrs != nil && mediaType.IsSameContentType(objectAttrs.ContentType) {
		logger.Printf("%v found for path %v", mediaType.String(), path)

		return path, nil
	}

	err := &StorageBucketNotFoundError{
		Path: path,
	}

	return "", err
}

func (s *StorageBucket) downloadURLForPath(path string, duration time.Duration) (string, error) {

	serviceAccountJSON, err := ioutil.ReadFile(os.Getenv("FIREBASE_SERVICE_ACCOUNT"))
	if err != nil {
		return "", err
	}

	conf, err := google.JWTConfigFromJSON(serviceAccountJSON)
	if err != nil {
		return "", err
	}

	logger.Printf("loaded configuration for email %v with expiration %v", conf.Email, conf.Expires)

	opts := &storage.SignedURLOptions{
		Scheme:         storage.SigningSchemeV4,
		Method:         "GET",
		GoogleAccessID: conf.Email,
		PrivateKey:     conf.PrivateKey,
		Expires:        time.Now().Add(duration),
	}

	url, err := storage.SignedURL(s.bucketName, path, opts)
	if err != nil {
		return "", err
	}

	logger.Printf("created url %s for path %s", url, path)

	return url, nil
}

func (s *StorageBucket) latestPathForLocation(location string) string {
	it := s.bucket.Objects(s.ctx, &storage.Query{
		Prefix:   location,
		Versions: false,
	})

	var highestName string
	var highestTime time.Time

	for {
		attr, err := it.Next()

		if err == iterator.Done {
			break
		}

		if err != nil {
			logger.Printf("[ERROR] - error while iterating %s", err)
			return ""
		}

		if strings.Contains(attr.Name, "thumb_") {
			continue
		}

		if attr.Updated.After(highestTime) {
			highestName = attr.Name
			highestTime = attr.Updated
		}

		logger.Printf("object %s created at %s updated at %s", attr.Name, attr.Created, attr.Updated)
	}

	return highestName
}
