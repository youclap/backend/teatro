package main

import (
	"strings"
	"time"
)

type MediaType int

const (
	IMAGE MediaType = iota
	VIDEO
)

func (m MediaType) String() string {
	return [...]string{"image", "video"}[m]
}

func (m MediaType) ContentType() string {
	return [...]string{"image", "video"}[m]
}

func (m MediaType) IsSameContentType(contentType string) bool {
	switch m {
	case IMAGE:
		return strings.Contains(contentType, m.ContentType())
	case VIDEO:
		return strings.Contains(contentType, "video") || strings.Contains(contentType, "application")
	default:
		logger.Printf("ERROR: unknown media type %v", m)
		return false
	}
}

func (m MediaType) CacheExpiration() time.Duration {
	switch m {
	case IMAGE:
		return 14 * 24 * time.Hour
	case VIDEO:
		return 7 * 24 * time.Hour
	default:
		logger.Printf("ERROR: unknown media type %v", m)
		return 0
	}
}
