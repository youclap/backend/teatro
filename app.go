package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"

	"cloud.google.com/go/storage"
	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/file"
)

// Global Variables
var (
	storageClient *storage.Client
	mainContext   context.Context

	logger *log.Logger
)

func main() {
	mainContext = context.Background()

	logger = log.New(os.Stdout, "teatro: ", log.LstdFlags)

	if err := setupStorageClient(mainContext); err != nil {
		logger.Fatalf("setup failed with error %v", err)
	}

	logger.Printf("Configuring routes...")

	router := mux.NewRouter()

	router.HandleFunc("/_ah/warmup", func(w http.ResponseWriter, r *http.Request) {
		logger.Println("warmup done")
	})

	router.HandleFunc("/healthcheck", healthcheck)

	imageRouter := router.
		Methods("GET").
		PathPrefix("/image").
		Subrouter()

	setupChallengeRoutes(imageRouter)

	setupUserRoutes(imageRouter)

	teatroRouter := router.
		Methods("GET").
		PathPrefix("/").
		Subrouter()

	setupChallengeRoutes(teatroRouter)

	setupUserRoutes(teatroRouter)

	http.Handle("/", router)

	appengine.Main()
}

func setupChallengeRoutes(r *mux.Router) {
	challengeRouter := r.PathPrefix("/challenge").Subrouter()

	challengeRouter.Path("/{challengeID}").
		HandlerFunc(handleChallenge(IMAGE)).
		Name("HandleChallenge")

	challengeRouter.Path("/{challengeID}").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handleChallenge(IMAGE)).
		Name("HandleChallengeWithWidth")

	challengeRouter.Path("/{challengeID}/post/{postID}").
		HandlerFunc(handlePost(IMAGE)).
		Name("HandlePost")

	challengeRouter.Path("/{challengeID}/post/{postID}").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handlePost(IMAGE)).
		Name("HandlePostWithWidth")

	// New Routes

	challengeRouter.Path("/{challengeID}/image").
		HandlerFunc(handleChallenge(IMAGE)).
		Name("HandleChallengeImage")

	challengeRouter.Path("/{challengeID}/image").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handleChallenge(IMAGE)).
		Name("HandleChallengeImageWithWidth")

	challengeRouter.Path("/{challengeID}/video").
		HandlerFunc(handleChallenge(VIDEO)).
		Name("HandleChallengeVideo")

	challengeRouter.Path("/{challengeID}/video").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handleChallenge(VIDEO)).
		Name("HandleChallengeVideoWithWidth")

	challengeRouter.Path("/{challengeID}/post/{postID}/image").
		HandlerFunc(handlePost(IMAGE)).
		Name("HandlePostImage")

	challengeRouter.Path("/{challengeID}/post/{postID}/image").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handlePost(IMAGE)).
		Name("HandlePostImageWithWidth")

	challengeRouter.Path("/{challengeID}/post/{postID}/video").
		HandlerFunc(handlePost(VIDEO)).
		Name("HandlePostVideo")

	challengeRouter.Path("/{challengeID}/post/{postID}/video").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handlePost(IMAGE)).
		Name("HandlePostVideoWithWidth")
}

func setupUserRoutes(r *mux.Router) {
	userRouter := r.PathPrefix("/user").Subrouter()

	userRouter.Path("/{userID}").
		HandlerFunc(handleUser).
		Name("HandleUser")

	userRouter.Path("/{userID}").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handleUser).
		Name("HandleUserWithWidth")

	userRouter.Path("/{userID}/{version}").
		HandlerFunc(handleUser).
		Name("HandleUserWithVersion")

	userRouter.Path("/{userID}/{version}").
		Queries("width", "{width:[0-9]+}").
		HandlerFunc(handleUser).
		Name("HandleUserWithVersionAndWidth")
}

func handleChallenge(mediaType MediaType) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		service, err := setupService(w, r)
		if err != nil {
			logger.Printf("ERROR: failed to create service with error %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		requestVars := mux.Vars(r)
		challengeID := requestVars["challengeID"]

		url, err := service.handleChallenge(challengeID, mediaType)

		handleURLOrError(url, err, mediaType, w, r)
	}
}

func handlePost(mediaType MediaType) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		service, err := setupService(w, r)
		if err != nil {
			logger.Printf("ERROR: failed to create service with error %v", err)
		}

		requestVars := mux.Vars(r)
		challengeID := requestVars["challengeID"]
		postID := requestVars["postID"]

		url, err := service.handlePost(challengeID, postID, mediaType)

		handleURLOrError(url, err, mediaType, w, r)
	}
}

func handleUser(w http.ResponseWriter, r *http.Request) {
	service, err := setupService(w, r)
	if err != nil {
		logger.Printf("ERROR: failed to create service with error %v", err)
	}

	requestVars := mux.Vars(r)
	userID := requestVars["userID"]
	version := requestVars["version"]

	url, err := service.handleUser(userID, version)

	handleURLOrError(url, err, IMAGE, w, r)
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	service, err := setupService(w, r)
	if err != nil {
		logger.Printf("ERROR: failed to create service with error %v", err)
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "using bucket name %v", service.storageBucket.bucketName)
}

// Warm up engines

func setupStorageClient(ctx context.Context) error {
	client, err := storage.NewClient(ctx)

	storageClient = client

	return err
}

func setupService(writer http.ResponseWriter, request *http.Request) (*Service, error) {
	requestContext := appengine.WithContext(mainContext, request)

	storageBucket, err := setupStorageBucket(requestContext)
	if err != nil {
		return nil, err
	}

	cache := setupCache(requestContext)

	return &Service{
		ctx:           requestContext,
		writer:        writer,
		cache:         cache,
		storageBucket: storageBucket,
	}, nil
}

func setupStorageBucket(ctx context.Context) (*StorageBucket, error) {
	defaultBucketName, err := file.DefaultBucketName(ctx)
	if err != nil {
		return nil, err
	}

	return &StorageBucket{
		client:     storageClient,
		bucket:     storageClient.Bucket(defaultBucketName),
		bucketName: defaultBucketName,
		ctx:        ctx,
	}, nil
}

func setupCache(ctx context.Context) *Cache {
	return &Cache{ctx: ctx}
}

func handleURLOrError(url *url.URL, err error, mediaType MediaType, w http.ResponseWriter, r *http.Request) {
	if err != nil {
		logger.Printf("ERROR: failed to get url with error %v", err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	switch mediaType {
	case IMAGE:
		populateURLWithRequestQueryParameters(url, r)
	case VIDEO:
		break
	}

	logger.Printf("Redirecting to URL: %v", url.String())

	http.Redirect(w, r, url.String(), http.StatusFound)
}

func populateURLWithRequestQueryParameters(url *url.URL, r *http.Request) {
	queryParameters := r.URL.Query()

	width, hasWidth := queryParameters["width"]
	if hasWidth {
		url.Path = fmt.Sprintf("%s-w%s", url.Path, width[0])
	} else {
		url.Path = fmt.Sprintf("%s-s0", url.Path)
	}

	format, hasFormat := queryParameters["format"]
	if hasFormat {
		switch format[0] {
		case "jpg":
			url.Path = fmt.Sprintf("%s-rj", url.Path)
		case "png":
			url.Path = fmt.Sprintf("%s-rp", url.Path)
		case "webp":
			url.Path = fmt.Sprintf("%s-rw", url.Path)
		}
	}
}
