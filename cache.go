package main

import (
	"context"
	"net/url"
	"time"

	"google.golang.org/appengine/memcache"
)

type Cache struct {
	ctx context.Context
}

func (c *Cache) urlForPath(path string) (*url.URL, error) {
	item, err := memcache.Get(c.ctx, path)
	if err != nil {
		logger.Printf("ERROR: failed to get content for path %v with error %v", path, err)
		return nil, err
	}

	urlString := string(item.Value)
	logger.Printf("Found url %v for path %v", urlString, path)

	return url.Parse(urlString)
}

func (c *Cache) storeURLForPath(url *url.URL, path string, duration time.Duration) {
	go func(ctx context.Context, url string, path string) {
		logger.Printf("storing url %v for path %v ⏳", url, path)

		item := &memcache.Item{
			Key:        path,
			Value:      []byte(url),
			Expiration: duration,
		}
		memcache.Add(c.ctx, item)

		logger.Printf("stored url %v for path %v 🚀", url, path)
	}(c.ctx, url.String(), path)
}
