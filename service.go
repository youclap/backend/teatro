package main

import (
	"context"
	"fmt"
	"io"
	"net/url"
	"time"

	"google.golang.org/appengine/blobstore"
	"google.golang.org/appengine/image"
)

const (
	CHALLENGE_IMAGE_PATH = "challenge/%v"
	POST_IMAGE_PATH      = "challenge/%v/post/%v"
	USER_IMAGE_PATH      = "user/%v/%v"
	USER_PATH            = "user/%v"
)

type Service struct {
	ctx    context.Context
	writer io.Writer

	cache         *Cache
	storageBucket *StorageBucket
}

func (s *Service) handleChallenge(challengeID string, mediaType MediaType) (*url.URL, error) {
	location := fmt.Sprintf(CHALLENGE_IMAGE_PATH, challengeID)

	return s.urlForLocationAndMediaType(location, mediaType, s.cache.urlForPath, s.buildPathForLocationAndMediaType, s.cache.storeURLForPath)
}

func (s *Service) handlePost(challengeID string, postID string, mediaType MediaType) (*url.URL, error) {
	location := fmt.Sprintf(POST_IMAGE_PATH, challengeID, postID)

	return s.urlForLocationAndMediaType(location, mediaType, s.cache.urlForPath, s.buildPathForLocationAndMediaType, s.cache.storeURLForPath)
}

func (s *Service) handleUser(userID string, version string) (*url.URL, error) {
	logger.Printf("Building location for user %v version %v", userID, version)

	location := fmt.Sprintf(USER_PATH, userID)
	latestLocation := s.storageBucket.latestPathForLocation(location)
	return s.urlForLocationAndMediaType(latestLocation, IMAGE, doNotSearchCache, s.buildPathForUser, doNotCache)
}

func (s *Service) urlForLocationAndMediaType(
	location string,
	mediaType MediaType,
	readFromCache func(string) (*url.URL, error),
	urlForLocationAndMedia func(string, MediaType, time.Duration) (*url.URL, error),
	storeInCache func(*url.URL, string, time.Duration)) (*url.URL, error) {

	cachePath := fmt.Sprintf("%v/%v", location, mediaType.String())

	cachedURL, err := readFromCache(cachePath)
	if err != nil {
		logger.Printf("not found cached URL for path %v", cachePath)
	} else if cachedURL != nil {
		return cachedURL, nil
	}

	duration := mediaType.CacheExpiration()

	url, err := urlForLocationAndMedia(location, mediaType, duration)
	if err != nil {
		return nil, err
	}

	storeInCache(url, cachePath, duration)

	return url, nil
}

func (s *Service) buildPathForLocationAndMediaType(location string, mediaType MediaType, duration time.Duration) (*url.URL, error) {
	mediaTypePath := fmt.Sprintf("%v/%v", location, mediaType.String())
	url, _ := s.urlForBucketPathAndMediaType(mediaTypePath, mediaType, duration)
	if url != nil {
		return url, nil
	}

	// fallback to media path
	mediaPath := fmt.Sprintf("%v/media", location)
	url, _ = s.urlForBucketPathAndMediaType(mediaPath, mediaType, duration)
	if url != nil {
		return url, nil
	}

	// fallback to thumb_media path
	thumbMediaPath := fmt.Sprintf("%v/thumb_media", location)
	return s.urlForBucketPathAndMediaType(thumbMediaPath, mediaType, duration)
}

func (s *Service) buildPathForUser(location string, mediaType MediaType, duration time.Duration) (*url.URL, error) {
	path := s.storageBucket.objectBucketPath(location)

	return s.urlForPath(path)
}

func (s *Service) urlForBucketPathAndMediaType(path string, mediaType MediaType, duration time.Duration) (*url.URL, error) {
	logger.Printf("url for %s at location %s", mediaType.String(), path)

	storagePath, err := s.storageBucket.objectBucketPathForPathMediaType(path, mediaType)
	if err != nil {
		logger.Printf("ERROR: failed to get storage path for %v media type %v with error %v", path, mediaType, err)
		return nil, err
	}

	var url *url.URL

	switch mediaType {
	case IMAGE:
		storagePath = s.storageBucket.objectBucketPath(storagePath)
		url, err = s.urlForPath(storagePath)
		if err != nil {
			logger.Printf("ERROR: failed to get url for storage path %v media type %v with error %v", storagePath, mediaType, err)
			return nil, err
		}
	case VIDEO:
		logger.Printf("generating url for video at location %v", path)
		urlAsString, err := s.storageBucket.downloadURLForPath(storagePath, duration)
		if err != nil {
			logger.Printf("ERROR: failed to get download url for %v with error %v", path, err)
			return nil, err
		}
		url, err = url.Parse(urlAsString)
		if err != nil {
			logger.Printf("ERROR: failed to parse download url for %v with error %v", path, err)
			return nil, err
		}
	}

	logger.Printf("generated url %v for path %s with media type %s", url, path, mediaType.String())

	return url, nil
}

func (s *Service) urlForPath(path string) (*url.URL, error) {
	logger.Printf("Fetching URL for path /%v\n", path)

	blobKey, err := blobstore.BlobKeyForFile(s.ctx, path)
	if err != nil {
		return nil, err
	}

	servingOptions := &image.ServingURLOptions{
		Secure: true,
	}

	googleURL, err := image.ServingURL(s.ctx, blobKey, servingOptions)
	if err != nil {
		return nil, err
	}

	// Add default modifiers to returned URL
	// Modifier to set cache-control header to 14 days
	// Modifier to not upscale image
	googleURL.Path = fmt.Sprintf("%s=e14-nu", googleURL.Path)

	return googleURL, nil
}

// Helper methods

func doNotSearchCache(string) (*url.URL, error) { return nil, nil }

func doNotCache(*url.URL, string, time.Duration) {}
