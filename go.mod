module gitlab.com/youclap/backend/teatro

go 1.11

require (
	cloud.google.com/go v0.43.0
	github.com/gorilla/mux v1.7.3
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	google.golang.org/api v0.7.0
	google.golang.org/appengine v1.6.1
)
